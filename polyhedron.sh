#!/bin/bash


# Functions
InverseWinkel() {
    # input: yaw, pitch, roll (in deg) on input image
    # output: yaw, pitch, roll for output image
    awk 'BEGIN {grad=180/3.141592654; OFMT="%f"; CONVFMT="%f"}
        1 {y=$1/grad;p=$2/grad;r=$3/grad;
        sinP=-sin(y)*sin(r)-cos(y)*sin(p)*cos(r);
        if (sinP==1) P=90/grad; 
        else { if (sinP==-1) P=-90/grad;
            else { tanP=sinP/sqrt(1-sinP^2); P=atan2(tanP,1); }}
        Y=atan2(-sin(y)*cos(r)+cos(y)*sin(p)*sin(r), cos(y)*cos(p))
        R=atan2(-cos(y)*sin(r)+sin(y)*sin(p)*cos(r), cos(p)*cos(r))
        print(Y*grad,P*grad,R*grad)}' < /dev/stdin;
}

Usage() {
    echo 
    echo ${0##*/} version 0.1:
    echo
    echo Generates pattern a Polyhedron out of 360x180 degree cylyndrical projection map.
    echo
    echo Usage: ${0##*/} Image
    echo
}


# Startup

if [[ "$#" -eq 0 ]]; then
    Usage
    exit 0
fi

nona='nona'

input_file=$1
prefix=$(echo "$input_file" | cut -f1 -d'.' )

dimentions=$(identify -format '%w %h' "$input_file")

input_width=$(echo $dimentions | cut -f1 -d' ')
input_height=$(echo $dimentions | cut -f2 -d' ')

output_size=512
output_t_height=443 # height = width*3/2/sqrt(3)

echo Converting image ${input_file} to Philosphere
echo
echo Image file: ${input_file}
echo Image size: ${input_width} x ${input_height} px
echo Face size: ${output_size} px
echo Output size: $(( $output_size * 8 )) x $(( $output_size * 5 )) px
echo


# Global constants

p="p f0"
m="m g1 i2"
o="o f4 w$input_width h$input_height v360 -s1 -u0"


# Extraction functions

extract_triangle () {
    lattice=$1
    yaw=$2
    pitch=$3
    roll=$4
    view=$5
    dir=$6
  
    read yaw pitch roll << eof
        `echo "$yaw $pitch $roll" | InverseWinkel;`
eof
        
    cat > "$tmp" << eof
$p w$output_size h$output_size v$view
$m
$o y$yaw p$pitch r$roll n"${input_file}"
eof

    echo Converting image T1-L$lattice $dir

    if [[ $dir -eq 1 ]]; then
        mask_img="mask-512-up.png"
        geometry="+0+0"
    else
        # Crop lower part of the projection so that p and v parameters
        # for it would be inverted parameters for the upper lattice
        mask_img="mask-512-down.png"
        geometry="+0+69"
    fi

    "$nona" -m PNG -o "${prefix}-T1-L${lattice}.png" "$tmp"
    convert "${prefix}-T1-L${lattice}.png" ${mask_img} \
        -geometry ${geometry} -compose CopyOpacity -composite "${prefix}-T1-L${lattice}.png"
}

extract_triangle_data () {
    triangle_data="$1"

    IFS=$'\n'
    for line in $triangle_data; do
    IFS=' '
    read lattice yaw pitch roll view dir << eof
    `echo "$line" | awk 'BEGIN {RS=" ";} NR==1 {next;} 
        /^L/ {L=substr($0, 2)}
        /^y/ {y=substr($0, 2)}
        /^p/ {p=substr($0, 2)}
        /^r/ {r=substr($0, 2)}
        /^v/ {v=substr($0, 2)}
        /^d/ {d=substr($0, 2)}
        END {print(L, y, p, r, v, d);}
    ';`
eof

    extract_triangle $lattice $yaw $pitch $roll $view $dir

    done
}

extract_square () {
    lattice=$1
    yaw=$2
    pitch=$3
    roll=$4
    view=$5
    index=$6

    read yaw pitch roll << eof
        `echo "$yaw $pitch $roll" | InverseWinkel;`
eof
        
    cat > "$tmp" << eof
$p w$output_size h$output_size v$view
$m
$o y$yaw p$pitch r$roll n"${input_file}"
eof

    echo Converting image S${index}-L$lattice

    "$nona" -m PNG -o "${prefix}-S${index}-L${lattice}.png" "$tmp"
}

extract_square_data () {
    square_data="$1"
    index="$2"

    IFS=$'\n'
    for line in $square_data; do
    IFS=' '
    read lattice yaw pitch roll view << eof
    `echo "$line" | awk 'BEGIN {RS=" ";} NR==1 {next;} 
        /^L/ {L=substr($0, 2)}
        /^y/ {y=substr($0, 2)}
        /^p/ {p=substr($0, 2)}
        /^r/ {r=substr($0, 2)}
        /^v/ {v=substr($0, 2)}
        END {print(L, y, p, r, v, d);}
    ';`
eof

    extract_square $lattice $yaw $pitch $roll $view $index

    done 
}


# Setup TMP files

tmp="$$.oto"
trap 'rm -f $tmp' 0


# Triangular faces

p_tr_n="38.5"
p_tr_s="-38.5"
v_tr="42.8481"

triangle_data="T1 L1 r0 p${p_tr_n} y-225 v${v_tr} d1
T1 L2 r0 p${p_tr_n} y-135 v${v_tr} d1
T1 L3 r0 p${p_tr_n} y-45 v${v_tr} d1
T1 L4 r0 p${p_tr_s} y-225 v${v_tr} d2
T1 L5 r0 p${p_tr_s} y-135 v${v_tr} d2
T1 L6 r0 p${p_tr_s} y-45 v${v_tr} d2
T1 L7 r0 p${p_tr_n} y45 v${v_tr} d1
T1 L8 r0 p${p_tr_s} y45 v${v_tr} d2"

extract_triangle_data "$triangle_data"


# Square faces part 1

square_data="S1 L1 r0 p0 y-225 v45
S1 L2 r0 p0 y-180 v45
S1 L3 r0 p0 y-135 v45
S1 L4 r0 p0 y-90 v45
S1 L5 r0 p0 y-45 v45
S1 L6 r0 p45 y-180 v45
S1 L7 r0 p45 y-90 v45
S1 L8 r0 p-45 y-180 v45
S1 L9 r0 p-45 y-90 v45"

extract_square_data "$square_data" 1


# Square faces part 2

square_data="S2 L1 r0 p0 y0 v45
S2 L2 r0 p0 y45 v45
S2 L3 r0 p0 y90 v45
S2 L4 r0 p45 y0 v45
S2 L5 r0 p45 y90 v45
S2 L6 r0 p-45 y0 v45
S2 L7 r0 p-45 y90 v45
S2 L8 r0 p90 y0 v45
S2 L9 r0 p-90 y0 v45"

extract_square_data "$square_data" 2


# Assemble Sheet 1

echo Creating image of Sheet 1 ${prefix}-S1.png

convert -size $(( $output_size * 5 ))x$(( $output_size * 3 )) xc:None \
  "${prefix}-T1-L1.png" -geometry +$(( 0                ))+$(( $output_size - $output_t_height )) -composite \
  "${prefix}-S1-L1.png" -geometry +$(( 0                ))+$(( $output_size     )) -composite \
  "${prefix}-T1-L4.png" -geometry +$(( 0                ))+$(( $output_size + $output_t_height )) -composite \
  "${prefix}-S1-L6.png" -geometry +$(( $output_size     ))+$(( 0                )) -composite \
  "${prefix}-S1-L2.png" -geometry +$(( $output_size     ))+$(( $output_size     )) -composite \
  "${prefix}-S1-L8.png" -geometry +$(( $output_size     ))+$(( $output_size * 2 )) -composite \
  "${prefix}-T1-L2.png" -geometry +$(( $output_size * 2 ))+$(( $output_size - $output_t_height )) -composite \
  "${prefix}-S1-L3.png" -geometry +$(( $output_size * 2 ))+$(( $output_size     )) -composite \
  "${prefix}-T1-L5.png" -geometry +$(( $output_size * 2 ))+$(( $output_size + $output_t_height )) -composite \
  "${prefix}-S1-L7.png" -geometry +$(( $output_size * 3 ))+$(( 0                )) -composite \
  "${prefix}-S1-L4.png" -geometry +$(( $output_size * 3 ))+$(( $output_size     )) -composite \
  "${prefix}-S1-L9.png" -geometry +$(( $output_size * 3 ))+$(( $output_size * 2 )) -composite \
  "${prefix}-T1-L3.png" -geometry +$(( $output_size * 4 ))+$(( $output_size - $output_t_height )) -composite \
  "${prefix}-S1-L5.png" -geometry +$(( $output_size * 4 ))+$(( $output_size     )) -composite \
  "${prefix}-T1-L6.png" -geometry +$(( $output_size * 4 ))+$(( $output_size + $output_t_height )) -composite \
  "${prefix}-S1.png"


# Assemble Sheet 2

echo Creating image of Sheet 2 ${prefix}-S2.png

convert -size $(( $output_size * 3 ))x$(( $output_size * 5 )) xc:None \
  "${prefix}-S2-L8.png" -geometry +0+$(( $output_size * 0 )) -composite \
  "${prefix}-S2-L4.png" -geometry +0+$(( $output_size * 1 )) -composite \
  "${prefix}-S2-L1.png" -geometry +0+$(( $output_size * 2 )) -composite \
  "${prefix}-S2-L6.png" -geometry +0+$(( $output_size * 3 )) -composite \
  "${prefix}-S2-L9.png" -geometry +0+$(( $output_size * 4 )) -composite \
  "${prefix}-T1-L7.png" -geometry +$(( $output_size ))+$(( $output_size * 2 - $output_t_height )) -composite \
  "${prefix}-S2-L2.png" -geometry +$(( $output_size ))+$(( $output_size * 2 )) -composite \
  "${prefix}-T1-L8.png" -geometry +$(( $output_size ))+$(( $output_size * 2 + $output_t_height )) -composite \
  "${prefix}-S2-L5.png" -geometry +$(( $output_size * 2 ))+$(( $output_size * 1 )) -composite \
  "${prefix}-S2-L3.png" -geometry +$(( $output_size * 2 ))+$(( $output_size * 2 )) -composite \
  "${prefix}-S2-L7.png" -geometry +$(( $output_size * 2 ))+$(( $output_size * 3 )) -composite \
  "${prefix}-S2.png"

  
# Assemble final sheet

echo Creating image of Final Sheet ${prefix}-Philosphere.png

convert -size $(( $output_size * 8 ))x$(( $output_size * 5 )) xc:None \
    "${prefix}-S2.png" -geometry +0+0 -composite \
    "${prefix}-S1.png" -geometry +$(( $output_size * 3 ))+$(( $output_size )) -composite \
    "${prefix}-Philosphere.png"


# Cleanup

rm -f ${prefix}-T1-L*.png
rm -f ${prefix}-S1-L*.png
rm -f ${prefix}-S2-L*.png

