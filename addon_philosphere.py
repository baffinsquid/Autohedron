bl_info = {
    "name": "New Philosphere",
    "author": "BaffinSquid",
    "version": (1, 2),
    "blender": (2, 80, 0),
    "location": "View3D > Add > Mesh > New Philosphere",
    "description": "Adds a new Philosphere object (rhombicuboctahedron)",
    "warning": "",
    "doc_url": "",
    "category": "Add Mesh",
}


import bpy
from bpy.types import Operator
from bpy.props import FloatVectorProperty
from bpy_extras.object_utils import AddObjectHelper, object_data_add
from mathutils import Vector
from math import sqrt


def add_object(self, context):
    # Generate vertices
    
    # Cartesian coordinates for the vertices of a rhombicuboctahedron centred at the origin,
    # with edge length 2 units, are all the even permutations of
    # (+-1, +-1, +-(1+sqrt(2)))

    verts = []

    A = 1+sqrt(2)

    COORDS = [(-1,-1), (1,-1), (1,1), (-1,1)]

    # Top square
    for (i,j) in COORDS:
        x = -j * 1.0
        y = i * 1.0
        z = A
        verts.append(Vector( (x * self.scale.x, y * self.scale.y, z * self.scale.z) ))

    # Bottom square
    for (i,j) in COORDS:
        x = j * 1.0
        y = i * 1.0
        z = -A
        verts.append(Vector( (x * self.scale.x, y * self.scale.y, z * self.scale.z) ))

    # Front square
    for (i,j) in COORDS:
        x = A
        y = i * 1.0
        z = j * 1.0
        verts.append(Vector( (x * self.scale.x, y * self.scale.y, z * self.scale.z) ))

    # Right square
    for (i,j) in COORDS:
        x = -i * 1.0
        y = A
        z = j * 1.0
        verts.append(Vector( (x * self.scale.x, y * self.scale.y, z * self.scale.z) ))

    # Back square
    for (i,j) in COORDS:
        x = -A
        y = -i * 1.0
        z = j * 1.0
        verts.append(Vector( (x * self.scale.x, y * self.scale.y, z * self.scale.z) ))

    # Left square
    for (i,j) in COORDS:
        x = i * 1.0
        y = -A
        z = j * 1.0
        verts.append(Vector( (x * self.scale.x, y * self.scale.y, z * self.scale.z) ))
    

    #
    # Start mesh generation
    #
    faces = []
    edges = []

    seam_edges = []

    # Top and bottom squares
    for k in [0,4]:
        faces.append([k, k+1, k+2, k+3])

    for k in [0,4]:
        for l in range(3):
            seam_edges.append([k+l,k+l+1])

    # Main side squares
    for k in [8,12,16,20]:
        faces.append([k, k+1, k+2, k+3])

    k = 20
    seam_edges.append([k,k+3])

    # Intermediate side squares
    for k in [8,12,16,20]:
        l=k+4 if k<20 else 8 # Loop around
        faces.append([k+1, l, l+3, k+2])


    # Intermediate top square edges
    for k in range(4):
        l = 8 + k * 4
        faces.append([l+3, l+2, (k+1) % 4, k])

    # Top triangles
    for k in range(4):
        l = 8 + k * 4
        m = (l+4) if k<3 else 8

        (a, b, c) = (l+2, m+3, (k+1) % 4)
        faces.append([a, b, c])

        seam_edges.append([c, a])
        seam_edges.append([c, b])


    # Intermediate bottom squares
    l0 = 8
    for k in range(4):
        l = (l0-1) - k
        m = l0 + k*4
        faces.append([l, (l-1) if k<3 else (l0-1), m+1, m])
    
    # Bottom triangles
    for k in range(4):
        m = 12 + k * 4

        a, b, c = 4+((2-k) if k<3 else 3), m if k<3 else (m-16), (m-4)+1
        faces.append([a, b, c])

        seam_edges.append([a, c])
        seam_edges.append([a, b])


    #
    # Finish mesh generation
    #

    mesh = bpy.data.meshes.new(name="New Philosphere")
    mesh.from_pydata(verts, edges, faces)
    # useful for development when the mesh may be invalid.
    # mesh.validate(verbose=True)

    for e in mesh.edges:
        if list(e.vertices) in seam_edges:
            e.use_seam = True

    object_data_add(context, mesh, operator=self)


class OBJECT_OT_add_object(Operator, AddObjectHelper):
    """Create a new Philosphere"""
    bl_idname = "mesh.add_philosphere"
    bl_label = "Add Philosphere"
    bl_options = {'REGISTER', 'UNDO'}

    scale: FloatVectorProperty(
        name="scale",
        default=(1.0, 1.0, 1.0),
        subtype='TRANSLATION',
        description="scaling",
    )

    def execute(self, context):

        add_object(self, context)

        return {'FINISHED'}


# Registration

def add_object_button(self, context):
    self.layout.operator(
        OBJECT_OT_add_object.bl_idname,
        text="Add Philosphere",
        icon='PLUGIN')


# This allows you to right click on a button and link to documentation
def add_object_manual_map():
    url_manual_prefix = "https://docs.blender.org/manual/en/latest/"
    url_manual_mapping = (
        ("bpy.ops.mesh.add_object", "scene_layout/object/types.html"),
    )
    return url_manual_prefix, url_manual_mapping


def register():
    bpy.utils.register_class(OBJECT_OT_add_object)
    bpy.utils.register_manual_map(add_object_manual_map)
    bpy.types.VIEW3D_MT_mesh_add.append(add_object_button)


def unregister():
    bpy.utils.unregister_class(OBJECT_OT_add_object)
    bpy.utils.unregister_manual_map(add_object_manual_map)
    bpy.types.VIEW3D_MT_mesh_add.remove(add_object_button)


if __name__ == "__main__":
    register()
