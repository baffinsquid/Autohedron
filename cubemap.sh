#!/bin/bash


Usage() {
    echo 
    echo ${0##*/} version 0.1:
    echo
    echo Generates pattern of a Cube out of 360x180 degree cylyndrical projection map.
    echo
    echo Usage: ${0##*/} Image
    echo
}


# Startup

if [[ "$#" -eq 0 ]]; then
    Usage
    exit 0
fi

nona='nona'

input_file=$1
prefix=$(echo "$input_file" | cut -f1 -d'.' )

dimentions=$(identify -format '%w %h' "$input_file")

input_width=$(echo $dimentions | cut -f1 -d' ')
input_height=$(echo $dimentions | cut -f2 -d' ')

output_size=512


echo Converting image ${input_file} to Philosphere
echo
echo Image file: ${input_file}
echo Image size: ${input_width} x ${input_height} px
echo Face size: ${output_size} px
echo Output size: $(( $output_size * 4 ))x$(( $output_size * 3 )) px
echo


# Setup TMP files

tmp="$$.oto"
trap 'rm -f $tmp' 0


# Extract faces of a cube

p="p f0 w$output_size h$output_size v90"            # p-line describes rectilinear panorama on cube face
m="m g1 i2"                                         # m-line: gamma 1.0 spline36 interpolator
o="o f4 w$input_width h$input_height v360 -s1 -u0"  # o-line selects from spherical full panorama

cat > "$tmp" << eof
$p
$m
$o y0 p0 r0 n"${input_file}"
eof
"$nona" -m PNG -o "$prefix-front.png" "$tmp"

cat > "$tmp" << eof
$p
$m
$o y270 p0 r0 n"${input_file}"
eof
"$nona" -m PNG -o "$prefix-right.png" "$tmp"

cat > "$tmp" << eof
$p
$m
$o y180 p0 r0 n"${input_file}"
eof
"$nona" -m PNG -o "$prefix-back.png" "$tmp"

cat > "$tmp" << eof
$p
$m
$o y90 p0 r0 n"${input_file}"
eof
"$nona" -m PNG -o "$prefix-left.png" "$tmp"

cat > "$tmp" << eof
$p
$m
$o y0 p270 r0 n"${input_file}"
eof
"$nona" -m PNG -o "$prefix-top.png" "$tmp"

cat > "$tmp" << eof
$p
$m
$o y0 p90 r0 n"${input_file}"
eof
"$nona" -m PNG -o "$prefix-bottom.png" "$tmp"

  
# Assemble final sheet

echo Creating image of Final Sheet ${prefix}-Cubemap.png

convert -size $(( $output_size * 4 ))x$(( $output_size * 3 )) xc:None \
    "${prefix}-left.png"    -geometry +0+$(( $output_size )) -composite \
    "${prefix}-top.png"     -geometry +$(( $output_size ))+$(( 0 )) -composite \
    "${prefix}-front.png"   -geometry +$(( $output_size ))+$(( $output_size )) -composite \
    "${prefix}-bottom.png"  -geometry +$(( $output_size ))+$(( $output_size * 2 )) -composite \
    "${prefix}-right.png"   -geometry +$(( $output_size * 2 ))+$(( $output_size )) -composite \
    "${prefix}-back.png"    -geometry +$(( $output_size * 3 ))+$(( $output_size )) -composite \
    "${prefix}-Cubemap.png"
